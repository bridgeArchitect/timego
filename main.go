package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"time"
)

type TimeQuery struct {
	TypeQuery  int      `xml:"typeQuery"`
	TimeCity   string   `xml:"timeCity"`
	Time1      int64    `xml:"time1"`
	Time2  	   int64    `xml:"time2"`
}

const (
	tcp      = "tcp"
	port     = ":15000"
	logPath  = "/home/bridgearchitect/SP/Lab4/Server/logfile.txt"
	req1Path = "/home/bridgearchitect/SP/Lab4/Server/Request-1.xml"
	req2Path = "/home/bridgearchitect/SP/Lab4/Server/Request-2.xml"
	ans1Path = "/home/bridgearchitect/SP/Lab4/Server/Response-1.xml"
	ans2Path = "/home/bridgearchitect/SP/Lab4/Server/Response-2.xml"
)

const (
	size       = 1024
	cityKiev   = "Kiev"
	cityLond   = "London"
	typeOne    = 1
	typeBoth   = 2
	messBegin  = "Server starts working!"
)

var (
	logfile *os.File
)

/* function to handle error */
func handleError(err error) {

	/* check error */
	if err != nil {
		log.Fatal(err)
	}

}

/* write byte array to the file */
func writeArrayToFile(pathFile string, byteArray []byte) {

	var (
		err error
	)

	/* write byte array into file */
	err = ioutil.WriteFile(pathFile, byteArray, 0777)
	handleError(err)

}

/* function to handle connection */
func handleConn(conn net.Conn) {

	var (
		inputArray  []byte
		outputArray []byte
		err         error
		timeQuery   TimeQuery
		tUnix       int64
		n           int
	)

	/* close connection */
	defer conn.Close()

	/* read byte array */
	inputArray = make([]byte, size)
	n, err = conn.Read(inputArray)
	handleError(err)

	/* receive instance of structure from byte array */
	err = xml.Unmarshal(inputArray, &timeQuery)
	handleError(err)

	if timeQuery.TypeQuery == typeOne && timeQuery.TimeCity == cityLond {

		/* write input byte array into file */
		writeArrayToFile(req1Path, inputArray[0:n])

		/* receive and set London time */
		tUnix = time.Now().Unix()
		timeQuery.Time1 = tUnix

		/* give answer */
		outputArray, err = xml.MarshalIndent(timeQuery,"", " ")
		_, err = conn.Write(outputArray)
		handleError(err)

		/* write answer to the file */
		writeArrayToFile(ans1Path, outputArray)

	} else if timeQuery.TypeQuery == typeOne && timeQuery.TimeCity == cityKiev  {

		/* write input byte array into file */
		writeArrayToFile(req1Path, inputArray[0:n])

		/* receive and set Kiev time */
		tUnix = time.Now().Unix() + 7200
		timeQuery.Time1 = tUnix

		/* give answer */
		outputArray, err = xml.MarshalIndent(timeQuery,"", " ")
		_, err = conn.Write(outputArray)
		handleError(err)

		/* write answer to the file */
		writeArrayToFile(ans1Path, outputArray)

	} else if timeQuery.TypeQuery == typeBoth {

		/* write input byte array into file */
		writeArrayToFile(req2Path, inputArray[0:n])

		/* receive and set London time */
		tUnix = time.Now().Unix()
		timeQuery.Time1 = tUnix

		/* receive and set Kiev time */
		tUnix = time.Now().Unix() + 7200
		timeQuery.Time2 = tUnix

		/* give answer */
		outputArray, err = xml.MarshalIndent(timeQuery,"", " ")
		_, err = conn.Write(outputArray)
		handleError(err)

		/* write answer to the file */
		writeArrayToFile(ans2Path, outputArray)

	}

}

func main() {

	var (
		listener net.Listener
		err      error
		conn     net.Conn
	)

	/* creation of listener to listen port */
	listener, err = net.Listen(tcp, port)
	handleError(err)
	defer listener.Close()

	/* creation of logfile */
	logfile, err = os.Create(logPath)
	handleError(err)

	/* write the first message */
	_, err = logfile.WriteString(messBegin + "\n")
	handleError(err)
	fmt.Println(messBegin)

	for {

		/* creation of connection */
		conn, err = listener.Accept()
		handleError(err)
		/* transfer connection processing to function */
		go handleConn(conn)

	}

}
